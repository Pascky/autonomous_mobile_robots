# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "perception: 4 messages, 0 services")

set(MSG_I_FLAGS "-Iperception:/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/indigo/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/indigo/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(perception_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg" NAME_WE)
add_custom_target(_perception_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "perception" "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg" "perception/Observation"
)

get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg" NAME_WE)
add_custom_target(_perception_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "perception" "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg" "geometry_msgs/Point"
)

get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg" NAME_WE)
add_custom_target(_perception_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "perception" "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg" "perception/Landmark:geometry_msgs/Point"
)

get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg" NAME_WE)
add_custom_target(_perception_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "perception" "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg" ""
)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg"
  "${MSG_I_FLAGS}"
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception
)
_generate_msg_cpp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception
)
_generate_msg_cpp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg"
  "${MSG_I_FLAGS}"
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception
)
_generate_msg_cpp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception
)

### Generating Services

### Generating Module File
_generate_module_cpp(perception
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(perception_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(perception_generate_messages perception_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg" NAME_WE)
add_dependencies(perception_generate_messages_cpp _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg" NAME_WE)
add_dependencies(perception_generate_messages_cpp _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg" NAME_WE)
add_dependencies(perception_generate_messages_cpp _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg" NAME_WE)
add_dependencies(perception_generate_messages_cpp _perception_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(perception_gencpp)
add_dependencies(perception_gencpp perception_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS perception_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg"
  "${MSG_I_FLAGS}"
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception
)
_generate_msg_lisp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception
)
_generate_msg_lisp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg"
  "${MSG_I_FLAGS}"
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception
)
_generate_msg_lisp(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception
)

### Generating Services

### Generating Module File
_generate_module_lisp(perception
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(perception_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(perception_generate_messages perception_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg" NAME_WE)
add_dependencies(perception_generate_messages_lisp _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg" NAME_WE)
add_dependencies(perception_generate_messages_lisp _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg" NAME_WE)
add_dependencies(perception_generate_messages_lisp _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg" NAME_WE)
add_dependencies(perception_generate_messages_lisp _perception_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(perception_genlisp)
add_dependencies(perception_genlisp perception_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS perception_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg"
  "${MSG_I_FLAGS}"
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception
)
_generate_msg_py(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception
)
_generate_msg_py(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg"
  "${MSG_I_FLAGS}"
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception
)
_generate_msg_py(perception
  "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception
)

### Generating Services

### Generating Module File
_generate_module_py(perception
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(perception_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(perception_generate_messages perception_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observations.msg" NAME_WE)
add_dependencies(perception_generate_messages_py _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmark.msg" NAME_WE)
add_dependencies(perception_generate_messages_py _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Landmarks.msg" NAME_WE)
add_dependencies(perception_generate_messages_py _perception_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sakhile/Documents/ECE_232/amr-workshops/src/perception/msg/Observation.msg" NAME_WE)
add_dependencies(perception_generate_messages_py _perception_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(perception_genpy)
add_dependencies(perception_genpy perception_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS perception_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/perception
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(perception_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(perception_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/perception
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(perception_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(perception_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/perception
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(perception_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(perception_generate_messages_py sensor_msgs_generate_messages_py)
endif()
