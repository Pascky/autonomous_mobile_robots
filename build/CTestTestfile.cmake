# CMake generated Testfile for 
# Source directory: /home/sakhile/Documents/ECE_232/amr-workshops/src
# Build directory: /home/sakhile/Documents/ECE_232/amr-workshops/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(executive)
subdirs(interprocess_communication)
subdirs(mapper)
subdirs(perception)
subdirs(gui)
subdirs(localization)
subdirs(pfc)
subdirs(planner)
subdirs(sim)
