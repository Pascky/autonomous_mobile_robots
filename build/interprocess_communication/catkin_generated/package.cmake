set(_CATKIN_CURRENT_PACKAGE "interprocess_communication")
set(interprocess_communication_VERSION "0.0.0")
set(interprocess_communication_MAINTAINER "smathunj <smathunj@todo.todo>")
set(interprocess_communication_PACKAGE_FORMAT "1")
set(interprocess_communication_BUILD_DEPENDS "message_generation" "roscpp" "rospy" "std_msgs")
set(interprocess_communication_BUILD_EXPORT_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs")
set(interprocess_communication_BUILDTOOL_DEPENDS "catkin")
set(interprocess_communication_BUILDTOOL_EXPORT_DEPENDS )
set(interprocess_communication_EXEC_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs")
set(interprocess_communication_RUN_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs")
set(interprocess_communication_TEST_DEPENDS )
set(interprocess_communication_DOC_DEPENDS )
set(interprocess_communication_DEPRECATED "")