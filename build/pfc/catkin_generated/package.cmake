set(_CATKIN_CURRENT_PACKAGE "pfc")
set(pfc_VERSION "0.0.0")
set(pfc_MAINTAINER "smathunj <smathunj@todo.todo>")
set(pfc_PACKAGE_FORMAT "1")
set(pfc_BUILD_DEPENDS "geometry_msgs" "message_generation" "nav_msgs" "roscpp" "rospy" "std_msgs" "std_msgs" "turtlebot_path_planner")
set(pfc_BUILD_EXPORT_DEPENDS "geometry_msgs" "message_runtime" "nav_msgs" "roscpp" "rospy" "std_msgs" "turtlebot_path_planner")
set(pfc_BUILDTOOL_DEPENDS "catkin")
set(pfc_BUILDTOOL_EXPORT_DEPENDS )
set(pfc_EXEC_DEPENDS "geometry_msgs" "message_runtime" "nav_msgs" "roscpp" "rospy" "std_msgs" "turtlebot_path_planner")
set(pfc_RUN_DEPENDS "geometry_msgs" "message_runtime" "nav_msgs" "roscpp" "rospy" "std_msgs" "turtlebot_path_planner")
set(pfc_TEST_DEPENDS )
set(pfc_DOC_DEPENDS )
set(pfc_DEPRECATED "")