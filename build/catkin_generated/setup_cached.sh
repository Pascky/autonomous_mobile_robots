#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/sakhile/Documents/ECE_232/amr-workshops/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/sakhile/Documents/ECE_232/amr-workshops/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/sakhile/Documents/ECE_232/amr-workshops/devel/lib:$LD_LIBRARY_PATH"
export PATH="/home/sakhile/Documents/ECE_232/amr-workshops/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/sakhile/Documents/ECE_232/amr-workshops/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/sakhile/Documents/ECE_232/amr-workshops/build"
export PYTHONPATH="/home/sakhile/Documents/ECE_232/amr-workshops/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/sakhile/Documents/ECE_232/amr-workshops/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/sakhile/Documents/ECE_232/amr-workshops/src:$ROS_PACKAGE_PATH"