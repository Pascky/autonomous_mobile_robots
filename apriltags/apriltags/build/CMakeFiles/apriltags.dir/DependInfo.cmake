# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/Edge.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/Edge.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/FloatImage.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/FloatImage.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/GLine2D.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/GLine2D.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/GLineSegment2D.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/GLineSegment2D.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/Gaussian.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/Gaussian.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/GrayModel.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/GrayModel.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/Homography33.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/Homography33.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/MathUtil.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/MathUtil.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/Quad.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/Quad.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/Segment.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/Segment.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/TagDetection.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/TagDetection.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/TagDetector.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/TagDetector.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/TagFamily.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/TagFamily.cc.o"
  "/home/smathunj/amr/amr-workshops/apriltags/apriltags/src/UnionFindSimple.cc" "/home/smathunj/amr/amr-workshops/apriltags/apriltags/build/CMakeFiles/apriltags.dir/src/UnionFindSimple.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/smathunj/amr/amr-workshops/devel/include"
  "../AprilTags"
  "../."
  "/opt/local/include"
  "/usr/include/opencv"
  "/usr/include/eigen3"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
