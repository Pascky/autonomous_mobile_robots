Name: Sakhile Mathunjwa
Course: Autonomous Mobile Robots
Project: 1

To compile all modules: 
1. Run the 'catkin_make' command in the 'amr/amr-workshops' folder. 
2. Next, run 'source devel/setup.bash'

To run a simulation of the software architecture:
1. First run the landmark publishing using the command './devel/lib/perception/landmark_publisher'
2. Then run the architecture as follows 'roslaunch project01_sim.launch'
3. Close the program using ^C, and using ^Z for the landmark publisher.

On the GUI, the landmarks are represented by red squares, and observed landmarks are represented by
blue squares. The blue circle with an arrow in it represents the robot goal(the orientation is 
irrelevant in this case), the black one represents the pose of the robot as computed by the simulator
/robot, and the green one represemts the pose of the robot as computed by the EKF state estimator.


Note that when the robot returns to the origin, the executive would have issued all the waypoints.

To run the software on a robot follow the same instructions except this time instead of using the 
script 'project01_sim.launch', use 'project01_robot.launch. 
