#include "ros/ros.h"
#include "geometry_msgs/Polygon.h"

using namespace std;
int main( int argc, char * argv[] ){
  ros::init( argc, argv, "waypoint_publisher" );
  ros::NodeHandle node_handle;
  ros::Publisher waypoints_publisher = node_handle.advertise< geometry_msgs::Polygon >( "waypoints", 1, true );

  sleep( 1 );

  cout << "publishing message" << endl;
  
  geometry_msgs::Polygon waypoints;

/* Project One waypoints
  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = -2.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.25;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = -2.0;
  waypoints.points.back().y = 1.25;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.25;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 0.0;
*/

/* Project Two waypoints */
  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;  
  waypoints.points.back().y = -2.0;  

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;  
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = -2.0;
  waypoints.points.back().y = 1.5;
  
  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 0.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = -2.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = -2.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 0.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = -2.0;


  //Graduate waypoints
/*   waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;  
  waypoints.points.back().y = -2.0;  

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;  
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = -5.0;
  waypoints.points.back().y = 1.5;
  
  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 0.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = -2.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = -5.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 1.5;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = 0.0;

  waypoints.points.push_back( geometry_msgs::Point32() );
  waypoints.points.back().x = 0.0;
  waypoints.points.back().y = -2.0;
*/


  double frequency = 10;
  ros::Rate timer(frequency);
  while( ros::ok ){
    waypoints_publisher.publish( waypoints );
    ros::spinOnce;
    timer.sleep();
  }

  cout << "done" << endl;

  return 0;
}

