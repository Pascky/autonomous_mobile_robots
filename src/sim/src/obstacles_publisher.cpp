#include <iostream>
#include "ros/ros.h"
#include "geometry_msgs/Polygon.h"

using namespace std;

int main( int argc, char* argv[] ){
  geometry_msgs::Polygon obstacles;

  // Project 1 practice obstacles
/*  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = -1.5;
  obstacles.points.back().y = 1.5; 
  obstacles.points.back().z = 1.0;

  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = -1.5;
  obstacles.points.back().y = 1.5;
  obstacles.points.back().z = 0.5;
 

  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = -1.5;
  obstacles.points.back().y = -1.5;
  obstacles.points.back().z = 0.5;

  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = 1.5;
  obstacles.points.back().y = -1.5;
  obstacles.points.back().z = 0.25;
*/
  // Project 1 practice obstacles
  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = 0.4;
  obstacles.points.back().y = 0.5;
  obstacles.points.back().z = 0.4;

  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = -1.0;
  obstacles.points.back().y = 1.5;
  obstacles.points.back().z = 0.25;
 

  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = 0.25;
  obstacles.points.back().y = -1.25;
  obstacles.points.back().z = 0.5;

  obstacles.points.push_back( geometry_msgs::Point32() );
  obstacles.points.back().x = 1.5;
  obstacles.points.back().y = -1.5;
  obstacles.points.back().z = 0.25;

  ros::init( argc, argv, "obstacles_publisher_node" );
  ros::NodeHandle node_handle;
  ros::Publisher obstacles_publisher = node_handle.advertise< geometry_msgs::Polygon >( "obstacles", 1, true );
  sleep( 1 );

  double frequency = 10;
  ros::Rate timer( frequency );
  while( ros::ok ){
    obstacles_publisher.publish( obstacles );
    timer.sleep();
  }

 return 0;
}
