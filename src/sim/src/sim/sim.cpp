#include <iostream>

#include "sim/sim.h"

using namespace std;

/*
 * x(0) = x_position
 * x(1) = y_position
 * x(2) = heading
 * u(0) = linear velocity (v)
 * u(1) = angular_velocity (w)
 */

geometry_msgs::Quaternion yaw_to_quaternion( const double& yaw ){
  geometry_msgs::Quaternion quaternion;
  quaternion.w = cos(yaw / 2.0);
  quaternion.x = 0.0;
  quaternion.y = 0.0;
  quaternion.z = sin( yaw / 2.0 );
  return quaternion;
}



double quaternion_to_yaw( const geometry_msgs::Quaternion& quaternion ){
  return atan2( 2.0 * ( quaternion.w * quaternion.z + quaternion.x * quaternion.y ), 1.0 - 2.0 *
         ( quaternion.y * quaternion.y + quaternion.z * quaternion.z ) );
}




Eigen::Vector2d relative_position( const Eigen::Vector3d robot,
                                        const perception::Landmark landmark){
  Eigen::Vector2d local;
  local(0) = cos( robot(2) ) * ( landmark.pos.x - robot(0) ) + sin( robot(2) ) * ( landmark.pos.y - robot(1) );
  local(1) = -sin( robot(2) ) * ( landmark.pos.x - robot(0) ) + cos( robot(2) ) * ( landmark.pos.y - robot(1) );
return local;
}


double sample ( const double& bsquared ){
  double b = sqrt(bsquared);
  double tmp = 0.0;
  for( int i = 0; i < 12 ; i++ ){
    tmp += -b + 2.0 * b * (double)(rand()%1000)/1000.0;
  }
  return 0.5 * tmp;
 
}

Sim::Sim(): _x(0.0, 0.0, 0.0), 
            _u(0.0, 0.0, 0.0 ),
            _alpha1( 0.005 ), 
            _alpha2( 0.05 ), 
            _alpha3( 0.005 ), 
            _alpha4( 0.05 ), 
            _alpha5( 0.005 ), 
            _alpha6( 0.05 ), 
            _t( 0.0 ), 
            _num_scan_angles( 128 ), 
            _num_scan_distances( 256 ),
            _observationMaxRange( 3.0 ),
            _observationMaxAngle( M_PI/2.0 ){
}



Sim::~Sim(){
}


void Sim::step ( const double& dt ){
  Eigen::Vector3d uhat( _u);
  uhat(0) = _u(0) + sample(_alpha1 * _u(0) * _u(0) + _alpha2 * _u(1) * _u(1));
  uhat(1) = _u(1) + sample(_alpha3 * _u(0) * _u(0) + _alpha4 * _u(1) * _u(1));
  
  Eigen::Vector3d dx( 0.0, 0.0, 0.0 );
  if( !uhat(1) ){
    dx(0) = uhat(0) * cos( _x(2) ) * dt;
    dx(1) = uhat(1) * sin( _x(2) ) * dt;
    dx(2) = uhat(2) * dt;
  
  }
  else{
   dx(0) = uhat(0)/uhat(1) * (-sin( _x(2) ) + sin( _x(2) + uhat(1) * dt ) );  
    dx(1) = uhat(0)/uhat(1) * (cos( _x(2) ) - cos( _x(2) + uhat(1) * dt ) );  
    dx(2) = uhat(1) * dt;  
  }

  _x +=dx;
  _t += dt;
 
   while( _x(2) > M_PI )
        _x(2) -= 2.0 * M_PI;

   while( _x(2) < -M_PI )
        _x(2) += 2.0 * M_PI;

  
  cout << "_u[3]: {" << _u(0) << ", " << _u(1) << ", " << _u(2) << "}" << endl;
  cout << "uhat[3]: {" << uhat(0) << ", " << uhat(1) << ", " << uhat(2) << ")" << endl;
  cout << "_x[3]: {" << _x(0) << ", " << _x(1) << ", " << _x(2) << "}" << endl;
  cout << "_t: " << _t << endl << endl;

  return;
}

sensor_msgs::LaserScan Sim::scan_msg( void ) const{
  sensor_msgs::LaserScan msg;
  msg.angle_min = -M_PI/2.0;
  msg.angle_max = M_PI/2.0;
  msg.angle_increment = ( msg.angle_max - msg.angle_min ) / ( double )( _num_scan_angles - 1 );
  msg.range_min = 0.45;
  msg.range_max = 10.0;
 
  // check to see if inside obstacle
  for( unsigned int i = 0; i < _obstacles.points.size(); i++ ){
    double distance_to_obstacle = ( Eigen::Vector2d( _x( 0 ), _x( 1 ) ) -
      Eigen::Vector2d( _obstacles.points[ i ].x, _obstacles.points[ i ].y ) ).norm();
    if( distance_to_obstacle < _obstacles.points[ i ].z ){
      return msg;
    }  
  }

  double range_increment = msg.range_max / ( double )( _num_scan_distances - 1 );

  Eigen::Vector2d robot_position( _x( 0 ), _x( 1 ) );

 // simulate the scans by checking for intersection with obstacles
  for( unsigned int i = 0; i < _num_scan_angles; i++ ){
    double angle = msg.angle_min + ( double )( i ) * msg.angle_increment;
    double min_range = msg.range_max;
    for( unsigned int j = 0; j < _num_scan_distances; j++ ){ 
      double range = ( double )( j ) * range_increment;
      Eigen::Vector2d scanpoint = robot_position + Eigen::Vector2d( range *
        cos( _x( 2 ) + angle ), range * sin( _x( 2 ) + angle ) );
      for( unsigned int k = 0; k < _obstacles.points.size(); k++ ){
        double distance_to_obstacle = ( scanpoint - Eigen::Vector2d(
          _obstacles.points[ k ].x, _obstacles.points[ k ].y ) ).norm();
        if( ( distance_to_obstacle < _obstacles.points[ k ].z ) && ( range < min_range ) ){
          min_range = range;
        }
      }
    }
    if( min_range > msg.range_min ){
      msg.ranges.push_back( std::min( min_range + sample( 0.001 ), (double)( msg.range_max ) ) );
    }
    else{
      msg.ranges.push_back( 0.0 );
    }
  }
  return msg;
}



void Sim::handle_obstacles( const geometry_msgs::Polygon::ConstPtr& msg ){
  _obstacles = *msg;
}


void Sim::handle_command( const geometry_msgs::Twist::ConstPtr& msg ){
  _u( 0 ) = msg->linear.x;
  _u( 1 ) = msg->angular.z;

  return;
}


void Sim::handle_landmarks( const perception::Landmarks::ConstPtr& msg ){
  _landmarks = *msg;
  cout << "Recieved " << _landmarks.landmarks.size() << " landmarks" << endl;
  return;
}


nav_msgs::Odometry Sim::odometry_msg( void ) const{
  nav_msgs::Odometry msg;
  msg.pose.pose.position.x = _x( 0 );
  msg.pose.pose.position.y = _x( 1 );
  msg.pose.pose.position.z = 0.0;
  msg.twist.twist.linear.x = _u(0);
  msg.twist.twist.angular.z = _u(1);
  msg.pose.pose.orientation = yaw_to_quaternion( _x( 2 ) );
  return msg;
}

void Sim::update_observed_landmarks( void ){
  update_observations();
  _observed_landmarks.landmarks.clear();
  perception::Landmark landmark;
  double range, angle;
  for( unsigned int i = 0; i <  _observations.observations.size(); i++){
    range = _observations.observations[i].range;
    angle = _x(2) + _observations.observations[i].bearing;

    landmark.pos.x = _x(0) + range * cos(angle);
    landmark.pos.y = _x(1) + range * sin(angle);
  
    _observed_landmarks.landmarks.push_back(landmark);  
  }
  cout << "Issued " << _observed_landmarks.landmarks.size() << " observed landmarks" << endl; 
  return;
};

void Sim::update_observations( void ){
  _observations.observations.clear(); 
  perception::Observation observation;
  double range, angle;
  for( unsigned int i = 0; i < _landmarks.landmarks.size(); i++){
    Eigen::Vector2d local = relative_position(_x, _landmarks.landmarks[i]);
    angle = atan2(local(1) , local(0)) + sample(0.0001);
    range = sqrt( pow(local(0), 2) + pow(local(1), 2) ) + sample( 0.0005 );
    if( (fabs(angle) < _observationMaxAngle) && (range < _observationMaxRange)){
      observation.range = range;
      observation.bearing = angle;
      observation.signature = _landmarks.landmarks[i].signature;
      _observations.observations.push_back( observation);
    }
  }
  cout << "Created " <<  _observations.observations.size() << " observations" << endl;
  return;
}

