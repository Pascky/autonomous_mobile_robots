#ifndef SIM_H
#define SIM_H

#define OBSERVATION_MAX_RANGE 3.0
#define OBSERVATION_MAX_BEARING M_PI*45/180

#include <Eigen/Dense>

#include "ros/ros.h"

#include "perception/Observations.h"
#include "perception/Landmarks.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/LaserScan.h"
#include "geometry_msgs/Polygon.h"


class Sim{

  public:
    Sim();
    virtual ~Sim();

    void step( const double& dt );
    void update_observed_landmarks( void );
    void update_observations( void );
    void handle_command( const geometry_msgs::Twist::ConstPtr& msg );
    void handle_landmarks( const perception::Landmarks::ConstPtr& msg );
    void handle_obstacles( const geometry_msgs::Polygon::ConstPtr& msg );    
    nav_msgs::Odometry odometry_msg( void ) const;
    sensor_msgs::LaserScan scan_msg( void ) const;
    perception::Landmarks& landmarks( void){ return _landmarks; };
    perception::Landmarks& observed_landmarks( void ){ return _observed_landmarks; };
    perception::Observations& observations( void ){ return _observations; };
    geometry_msgs::Polygon& obstacles( void ){ return _obstacles; };
 // perception::Observations observations_msg( void ) const;
 
  protected:
    Eigen::Vector3d _x;
    Eigen::Vector3d _u;
    double _alpha1;
    double _alpha2;
    double _alpha3;
    double _alpha4;
    double _alpha5;
    double _alpha6;
    double _t;
    unsigned _num_scan_angles;
    unsigned _num_scan_distances;
    perception::Landmarks _landmarks;
    perception::Landmarks _observed_landmarks;
    perception::Observations _observations;
    geometry_msgs::Polygon _obstacles;
    double _observationMaxRange;
    double _observationMaxAngle;
};

#endif /* SIM_H */
