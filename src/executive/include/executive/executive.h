#ifndef EXECUTIVE_H
#define EXECUTIVE_H

#define STR_DIST 0.25
#define ACCEPTABLE_GOAL_RANGE 0.1
#include "ros/ros.h"

#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Polygon.h"
#include "nav_msgs/Path.h"


class Executive{

  public:
    Executive();
    virtual ~Executive( void );

    void handle_odom( const nav_msgs::Odometry::ConstPtr& msg );
    void handle_path( const nav_msgs::Path::ConstPtr& msg );
    void handle_waypoints( const geometry_msgs::Polygon::ConstPtr& msg );
    ros::Publisher& goal_publisher(){ return _goal_publisher;}
    
  protected:
    geometry_msgs::Polygon _waypoints;
    nav_msgs::Odometry _odometry;
    double _distance_threshold;
    double _replan_distance_threshold;
    int _idx;
    ros::Publisher _goal_publisher;    

};
    


#endif /* EXECUTIVE_H */
