#include "ros/ros.h"

#include "executive.h"



int main( int argc, char* argv[] ){
  
  Executive executive;

  ros::init( argc, argv, "executive_node" );
  ros::NodeHandle node_handle;
  ros::Subscriber odom_subscriber = node_handle.subscribe( "estimated_odom",
    1, &Executive::handle_odom, &executive );
  ros::Subscriber path_subscriber = node_handle.subscribe( "path", 1, &Executive::handle_path, &executive );
// new subscriber
  ros::Subscriber waypoints_subscriber = node_handle.subscribe( "waypoints", 1, &Executive::handle_waypoints, &executive );
  executive.goal_publisher() = node_handle.advertise< geometry_msgs::Pose >( "goal", 1 );


  double frequency = 10.0;
  ros::Rate timer( frequency );
  while( ros::ok() ){
    ros::spinOnce();
    timer.sleep();
  }

  return 0;
}
