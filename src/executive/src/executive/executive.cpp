#include "executive/executive.h"

Executive::Executive(): _idx(0), 
                        _odometry(),
                        _distance_threshold(0.1),
                        _replan_distance_threshold(1.0){

}

Executive::~Executive(){
}



void Executive::handle_odom( const nav_msgs::Odometry::ConstPtr& msg ){
  _odometry = *msg;
  
  //If no waypoints or all waypoints exhausted, do nothing
  if( _idx < _waypoints.points.size() ){
    double dx = _odometry.pose.pose.position.x - _waypoints.points[_idx].x;
    double dy = _odometry.pose.pose.position.y - _waypoints.points[_idx].y;
    if(_idx == 0){
      //Update goal coordinates
      std::cout << "Publishing goal" << std::endl;
      geometry_msgs::Pose msg;
      msg.position.x = _waypoints.points[_idx].x;
      msg.position.y = _waypoints.points[_idx].y;   
      _goal_publisher.publish(msg);
      std::cout << "Index: " << _idx << std::endl;
    }
  
    if( sqrt( dx * dx + dy * dy) < _distance_threshold){
      //Update goal coordinates
      _idx++;
      if( _idx >= _waypoints.points.size() )
        return;
      std::cout << "Publishing goal" << std::endl;
      geometry_msgs::Pose msg;
      msg.position.x = _waypoints.points[_idx].x;
      msg.position.y = _waypoints.points[_idx].y;   
      _goal_publisher.publish(msg);
      std::cout << "Index: " << _idx << std::endl;
    }
  }
  return;
}



void Executive::handle_path( const nav_msgs::Path::ConstPtr& msg ){
  if( !_waypoints.points.empty() && (_idx < _waypoints.points.size()) ){
    double dx = _odometry.pose.pose.position.x - _waypoints.points[_idx].x;
    double dy = _odometry.pose.pose.position.y - _waypoints.points[_idx].y;
    if( sqrt( dx * dx + dy * dy ) > _replan_distance_threshold ){
      std::cout << "republshing goal " << _waypoints.points[_idx] << std::endl;
      geometry_msgs::Pose msg;
      msg.position.x = _waypoints.points[_idx].x;
      msg.position.y = _waypoints.points[_idx].y;
      _goal_publisher.publish( msg ); 
    }
  }
  return;
}



void Executive::handle_waypoints( const geometry_msgs::Polygon::ConstPtr& msg ){
  _waypoints = *msg;
  //std::cout << "Received " << _waypoints.points.size() << " waypoints" << std::endl;
//  _idx = 0;
  return;
}
