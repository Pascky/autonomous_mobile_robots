#include "localization/ekf_localization.h"


using namespace std;


geometry_msgs::Quaternion yaw_to_quaternion( const double& yaw ){
  geometry_msgs::Quaternion quaternion;
  quaternion.w = cos( yaw / 2.0 );
  quaternion.x = 0.0;
  quaternion.y = 0.0;
  quaternion.z = sin( yaw / 2.0 );
  return quaternion;
}


EKF_Localization::EKF_Localization( const Eigen::VectorXd& alpha, const
                                    Eigen::MatrixXd& q ) : _u(),
                                                           _landmarks(),
                                                           _z(),
                                                           _mu( Eigen::VectorXd::Zero( 3 ) ),
                                                           _sigma( Eigen::MatrixXd::Zero( 3, 3 ) ),
                                                           _alpha( alpha ),
                                                           _q( q ) {
}



EKF_Localization::~EKF_Localization() {
}


void EKF_Localization::handle_command( const geometry_msgs::Twist::ConstPtr& msg ){
  _u = *msg;
  return;
}


void EKF_Localization::handle_odometry( const nav_msgs::Odometry::ConstPtr& msg ){
  _u = msg->twist.twist;
  return;
}


void EKF_Localization::handle_landmarks( const perception::Landmarks::ConstPtr& msg ){
  for(unsigned int i = 0; i < msg->landmarks.size(); i++ ){
    map< int, geometry_msgs::Point >::iterator it_landmark = _landmarks.find(
       msg->landmarks[ i ].signature );
    if( it_landmark != _landmarks.end() ){
      it_landmark->second = msg->landmarks[ i ].pos;
    }
    else{
      _landmarks.insert( pair< int, geometry_msgs::Point >( msg->landmarks[ i ].signature, msg->landmarks[ i ].pos ) );
    }
  }
return;
}


void EKF_Localization::handle_observations( const perception::Observations::ConstPtr& msg ){
  _z = *msg;
  return;
}


void EKF_Localization::step( const double& dt ){
  // implement motion model step for time dt
  Eigen::MatrixXd G_t(3, 3), V_t(3, 2),  M_t(2, 2), Sigma_bar(3, 3);
  Eigen::MatrixXd H_t(3, 3),  S_t(3, 3), K_t(3, 3), I(3, 3);
  Eigen::Vector3d mu_bar, d_mu, z_bar, _z_vec, innovation;
  
  double v_t = _u.linear.x;
  double w_t = _u.angular.z;
  double theta = _mu(2);

  if( w_t == 0.0 )
    w_t = 0.0000001;

   G_t(0, 0) = 1;
   G_t(0, 1) = 0;
   G_t(0, 2) = -v_t/w_t * cos(theta) + v_t/w_t * cos(theta + w_t*dt);
   G_t(1, 0) = 0;
   G_t(1, 1) = 1;
   G_t(1, 2) = -v_t/w_t * sin(theta) + v_t/w_t * sin(theta + w_t*dt);
   G_t(2, 0) = 0;
   G_t(2, 1) = 0;
   G_t(2, 2) = 1;

  V_t(0, 0) = 1/w_t * (-sin(theta) + sin(theta + w_t*dt));
  V_t(0, 1) = v_t/pow(w_t, 2) * (sin(theta) - sin(theta + w_t*dt)) + v_t/w_t * cos(theta + w_t*dt)*dt;
  V_t(1, 0) = 1/w_t * (cos(theta) - cos(theta + w_t*dt));
  V_t(1, 1) = -v_t/pow(w_t, 2) * (cos(theta) - cos(theta + w_t*dt)) + v_t/w_t * sin(theta + w_t*dt) * dt;
  V_t(2, 0) = 0;
  V_t(2, 1) = dt;


  M_t(0, 0) = _alpha(0) * v_t * v_t + _alpha(1) * w_t * w_t;
  M_t(0, 1) = 0;
  M_t(1, 0) = 0;
  M_t(1, 1) = _alpha(2) * v_t * v_t + _alpha(3) * w_t * w_t;

  d_mu(0) = -v_t/w_t * sin(theta) + v_t/w_t * sin(theta + w_t*dt); 
  d_mu(1) = v_t/w_t * cos(theta) - v_t/w_t * cos(theta + w_t*dt);
  d_mu(2) = w_t*dt;

  mu_bar = _mu + d_mu;

  Sigma_bar = G_t * _sigma * G_t.transpose() + V_t * M_t * V_t.transpose();

  I << 1, 0 ,0,
       0, 1, 0,
       0, 0, 1;


  int j;
  double q, dx, dy;

  for( unsigned int i = 0; i < _z.observations.size(); i++ ){
  // implement measurement model step for all observations
    j = _z.observations[i].signature;
    map< int, geometry_msgs::Point >::iterator it = _landmarks.find( j );
    if( it != _landmarks.end() ){
      // found
      dx = it->second.x - mu_bar(0);
      dy = it->second.y - mu_bar(1);
     
      q = pow(dx, 2) + pow(dy, 2);
      z_bar(0) = sqrt(q);
      z_bar(1) = atan2(dy,  dx) - mu_bar(2);
      z_bar(2) = it->first;

      z_bar(1) = (fabs(z_bar(1)) < fabs(z_bar(1) + 2.0 * M_PI)) ? z_bar(1):z_bar(1) + 2.0 * M_PI;
      z_bar(1) = (fabs(z_bar(1)) < fabs(z_bar(1) - 2.0 * M_PI)) ? z_bar(1):z_bar(1) - 2.0 * M_PI;
      
     // cout << "Sim Obs[" << i << "] = (" << _z.observations[i].range << " , " << _z.observations[i].bearing * 180.0/M_PI << ", " << _z.observations[i].signature << ") " << endl; 
     // cout << "Loc Obs[" << i << "] = (" << z_bar(0) << " , " << z_bar(1) * 180/M_PI<< ", " << z_bar(2) << ") " << endl; 
      
      H_t(0, 0) = -dx/sqrt(q);
      H_t(0, 1) = -dy/sqrt(q);
      H_t(0, 2) = 0;
      H_t(1, 0) = dy/q;
      H_t(1, 1) = -dx/q;
      H_t(1, 2) = -1;
      H_t(2, 0) = 0;
      H_t(2, 1) = 0;
      H_t(2, 2) = 0;

      S_t = H_t * Sigma_bar * H_t.transpose() + _q;
      K_t = Sigma_bar * H_t.transpose() * S_t.inverse();
    
      _z_vec(0) = _z.observations[i].range; 
      _z_vec(1) = _z.observations[i].bearing;
      _z_vec(2) = _z.observations[i].signature;
      
      innovation = _z_vec - z_bar;
      if(innovation(2) > M_PI )
        innovation(2) -= 2.0 * M_PI;
      if(innovation(2) < -M_PI )
        innovation(2) +=  2.0 * M_PI;
      mu_bar = mu_bar +  K_t * innovation;
      Sigma_bar = (I - K_t * H_t)* Sigma_bar;
    }
  }
  while( mu_bar(2) > M_PI )
    mu_bar(2) -= 2.0 * M_PI;
  
  while( mu_bar(2) < -M_PI )
    mu_bar(2) += 2.0 * M_PI;

  _mu = mu_bar;

  _mu = mu_bar;
  _sigma = Sigma_bar;
 // cout << "Estinamed Pose(" << _mu(0) << ", " << _mu(1) << ", " << _mu(2) * 180.0/M_PI<< ") " << endl;

  // clear past observations
  _z.observations.clear();
  return;
}



nav_msgs::Odometry EKF_Localization::estimated_odometry( void ) const{
  nav_msgs::Odometry msg;
  msg.pose.pose.position.x = _mu( 0 );
  msg.pose.pose.position.y = _mu( 1 );
  msg.pose.pose.orientation = yaw_to_quaternion( _mu( 2 ) );
  
  return msg;
}
