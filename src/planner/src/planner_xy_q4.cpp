#include <iostream>
#include "ros/ros.h"
#include "planner/planner_xy.h"


using namespace std;

int main( int argc, char** argv ){
  
  geometry_msgs::Pose start, goal ;
  start.position.x = 5;
  start.position.y = 2;
  goal.position.x = -3;
  goal.position.y = 4;
  double w = 1.5;

  PlannerXY planner_xy = PlannerXY();
  ros::init( argc, argv, "planner_xy_q4");
  ros::NodeHandle node_handle1, node_handle2;
  ros::Publisher nav_msg_publisher = node_handle1.advertise< nav_msgs::Path > ( "path", 1, true );
  ros::Publisher ol_msg_publisher = node_handle2.advertise< std_msgs::UInt32 > ( "openlistsize", 1, true ); 
  ros::Publisher cl_msg_publisher = node_handle2.advertise< std_msgs::UInt32 > ( "closedlistsize", 1, true ); 


  nav_msgs::Path nav_msg; 
  std_msgs::UInt32 std_msg;
/*  double f = 10;
  ros::Rate loop_rate(f);
  while( ros::ok() ){ 
 if(*/ planner_xy.search(start, goal, (const double&) w );
// ){
     nav_msg = planner_xy.generate_path();
     nav_msg_publisher.publish( nav_msg );
  //}
  
  ol_msg_publisher.publish( planner_xy.open_list_size() );  
  cl_msg_publisher.publish( planner_xy.closed_list_size() );
  ros::spinOnce();
  //loop_rate.sleep( );
//  }
  
  return 0;

}
