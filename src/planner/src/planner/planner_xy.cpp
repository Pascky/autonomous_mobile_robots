
#include "planner/planner_xy.h"

/* Constructor */
PlannerXY::PlannerXY(){}


/* Destructor */
PlannerXY::~PlannerXY( void ){
//  mapper = *(new Mapper(0.25, 801, 801));
}



/* Return ValuE: Euclidean distance between two points */
double e_distance( Node& start, Node& end ){
  double dx = start.x - end.x;
  double dy = start.y - end.y;
  return sqrt(dx*dx + dy*dy);
}


/**
 * get_neighbours function
 * Used to populate a neigbhours array. This array contains all neighbours of node current freshly added to the closed list
 * @param Node neighbours[] is a array with a size equal to maximum possible neighbours. 
 * @param Node curr_node node whos neighbours we are trying to find
 * @param Node goal is the goal node. Used to evauate heuristic function for each neighbour.
 * param w is the weight given to the calue computed by the heuristic function
 * @param cl_size is the size of the closed list. Used for back pointing purposes
 * @return void
 */

/* Returns the neigbours of a given node */
void get_neighbours(Node neighbours[],  Node curr_node, Node goal, double w, int cl_size){
  
  //point to the left
  int i = 0;
  neighbours[i] = Node( curr_node.x - STR_DIST, curr_node.y, cl_size - 1, 0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + STR_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;
  
  
  //Top left
  i++;
  neighbours[i] = Node( curr_node.x - STR_DIST, curr_node.y + STR_DIST, cl_size - 1,  0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + DIAG_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;


  //Top
  i++;
  neighbours[i] = Node( curr_node.x, curr_node.y + STR_DIST, cl_size - 1,  0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + STR_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;

  
  //Top right
  i++;
  neighbours[i] = Node( curr_node.x + STR_DIST, curr_node.y + STR_DIST, cl_size - 1, 0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + DIAG_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;

  
  //Right
  i++;
  neighbours[i] = Node( curr_node.x + STR_DIST, curr_node.y, cl_size - 1,  0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + STR_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;


  //Bottom_right
  i++;
  neighbours[i] = Node( curr_node.x + STR_DIST, curr_node.y - STR_DIST, cl_size - 1, 0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + DIAG_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;


  //Bottom
  i++;
  neighbours[i] = Node( curr_node.x, curr_node.y - STR_DIST, cl_size - 1, 0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + STR_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h;
 
 
  //Bottom let
  i++;
  neighbours[i] = Node( curr_node.x - STR_DIST, curr_node.y - STR_DIST, cl_size - 1,  0.0, 0.0,  0.0 ); 
  neighbours[i].h = w * e_distance( neighbours[i], goal );
  neighbours[i].g = curr_node.g + DIAG_DIST;
  neighbours[i].f = neighbours[i].g + neighbours[i].h; 
  
  return;
}




/* contains function
 * Determines whether a given vecto contains a given node or not
 * @param std::vector<Node> v is the vector in which we are trying to find a particular node
 * @param Node n is the node that we are searching for in vector v
 * Return: -1 when node n not found, its index otherwise  
*/
int contains( std::vector<Node> v,  Node n ){
 for(int i = 0; i < v.size(); i++){
   if( (v.at(i).x ==  n.x) & (v.at(i).y == n.y) ){ 
      return i;
    }
 }
  return -1;
}



/*
 * compare struct
 * Defines how the min_element function is to make comparisons between give nodes
 * @param a and b are the two nodes to be compared
 * Return: true when a < b otherwise false
 */
struct compare{
  bool operator()( Node a, Node b){
    return a.f < b.f;
  }
} node_comparator;



/* 
 * search function
 * Implements the A* algorithm
 * @param const geometry_msgs::Pose& start is the start position and orientation 
 * @param const geometry_msgs::Pose& goal is the positiona we are trying to get to
 * @param const double& w is the weight to be used along with the heuristic function
 * Return: true if path found, otherwise false
 */
bool PlannerXY::search( const geometry_msgs::Pose& start, const geometry_msgs::Pose& goal, const double& w ){
  
   double start_x = round( start.position.x / STR_DIST ) * STR_DIST;
   double start_y = round( start.position.y / STR_DIST ) * STR_DIST;

   open_list.clear();     
   closed_list.clear();   
   int j; 
   Node pnode;  
   Node neighbours[N_NEIGHBOURS];
   std::vector< Node >::iterator  min_it;
   Node tmp_goal = Node( goal.position.x, goal.position.y, -1, 0, 0, 0 ); //Create node corresponding to goal pose
   double h = w * sqrt( (start.position.x - goal.position.x) * (start.position.x - goal.position.x) + (start.position.y - goal.position.y) *(start.position.y - goal.position.y)); //start pos's f
   open_list.push_back( Node(start_x, start_y, -1, h, 0, h ) ); //add start node to open list
   while( !open_list.empty() ){
     min_it = std::min_element(open_list.begin(), open_list.end(), node_comparator);  //get min element from vector
     std::iter_swap( min_it, open_list.end() - 1 );   //place min element at the very back of the vector (to make removal cheap)
     Node pnode = open_list.back();      
     open_list.pop_back();                      //remove min element from open list
     closed_list.push_back( pnode );           //add min element to closed list
     if( (pnode.x ==  goal.position.x) && (pnode.y == goal.position.y) ){   //exit with success if goal just pushed into closed list
       return true;
     }
    get_neighbours(neighbours, pnode, tmp_goal, w, closed_list.size());   
     for( int i = 0; i < N_NEIGHBOURS; i++){
      //set cost high if neighbour intersects with obstacle
//      std::cout << "(" << neighbours[i].x << ", " << neighbours[i].y << ") occupied: " << check_map(neighbours[i].x, neighbours[i].y) << std::endl;
      if( !check_map(neighbours[i].x, neighbours[i].y)){ 
        neighbours[i].f += 20.0; 
//        std::cout << "(" << neighbours[i].x << ", " << neighbours[i].y << ") occupied" << std::endl;    
      }
//       std::cout << "neighbour (" << neighbours[i].x << ", " << neighbours[i].y << ")" << std::endl; 
       if( (j = contains(open_list, neighbours[i])) != -1 ){   //if node in neighbour list found in open list, keep node version that has the lower f value 
           if( open_list.at(j).f > neighbours[i].f ){	   
            open_list.at(j).back_ptr = closed_list.size() - 1;
            open_list.at(j).f = neighbours[i].f;
            open_list.at(j).g = neighbours[i].g;
          } 
        }
        else if( (j = contains(closed_list, neighbours[i])) != -1 ){ //if node in neighbour list already in closed list, do nothing
        }   
        else{                             
          open_list.push_back( neighbours[i] );  //otherwise add to open list
        } 
      }
   }
   return false; //return false if algorithm does not get to goal state (no path)
}  
 



/**
 * generate_path function
 * Used to populate a nav_msgs::Path given a tree (in the form of a vector)
 * @param void takes no parameters. Uses the class's closed list instead
 * @return path the constructed nav_msgs::Path
 */
nav_msgs::Path PlannerXY::generate_path( void ){
  
  nav_msgs::Path path;  
  int curr_node_ptr = closed_list.size() - 1;
  geometry_msgs::PoseStamped pose;
  std::vector< geometry_msgs::PoseStamped > plan;
  int i = 0;
  while( curr_node_ptr != -1 ){ //use back pointers to trace path all the way back to start position
//    std::cout << "(" << closed_list.at( curr_node_ptr).x << ", " << closed_list.at( curr_node_ptr).y << ")" << "\t  f = " << closed_list.at(curr_node_ptr).f << "\t g = " << closed_list.at( curr_node_ptr).g << "\t h = " << closed_list.at( curr_node_ptr ).h  <<  " \t ptr = " << closed_list.at( curr_node_ptr ).back_ptr << std::endl;
    pose.pose.position.x = closed_list.at(curr_node_ptr ).x;
    pose.pose.position.y = closed_list.at( curr_node_ptr ).y;
    plan.push_back( pose );
    curr_node_ptr = closed_list.at( curr_node_ptr ).back_ptr;
  }
  std::reverse( plan.begin(), plan.end() ); 
  path.poses = plan; 
  return path;

}



/*
 * open_list_size function
 * Stores the size of the open list after the search process
 * @ param void takes no parameters
 * return msg the std_msgs::UInt32 containing the size of the open_list
 */
std_msgs::UInt32 PlannerXY::open_list_size( void ){
  std_msgs::UInt32 msg;
  msg.data = open_list.size();
  return msg;
}


/*
 * closed_list_size function
 * Stores the size of the open list after the search process
 * @ param void takes no parameters
 * return msg the std_msgs::UInt32 containing the size of the closed_list
 */
std_msgs::UInt32 PlannerXY::closed_list_size( void ){
  std_msgs::UInt32 msg;
  msg.data = closed_list.size();
  return msg;
}


/**

*/
void PlannerXY::handle_odom( const nav_msgs::Odometry::ConstPtr& msgs ){
  odometry = *msgs;
//  std::cout << "Odometry message recieved: (" << odometry.pose.pose.position.x << ", " << odometry.pose.pose.position.y << ")" << std::endl;
  return;
}


/*

*/
void PlannerXY::handle_goal( const geometry_msgs::Pose::ConstPtr& msg ){
   std::cout << "Goal message recieved: (" << (*msg).position.x << ", " << (*msg).position.y << ")" << std::endl;
   std::cout << "Start: " << odometry.pose.pose.position.x << ", " << odometry.pose.pose.position.y << ")" << std::endl;  
  if( search( odometry.pose.pose, *msg, 1) ){ 
    path_publisher.publish( generate_path() );
    closedlistsize_publisher.publish( closed_list_size() );
    openlistsize_publisher.publish( open_list_size() );
  }
  else{
    std::cout << " No path found!" << std::endl;
  }
  std::cout << "Done searching!" << std::endl;
  return;
}


bool PlannerXY::check_map( const double& x, const double& y ){
  //return mapper.checkMap( x * discretization, y * discretization, 0.25, 1.0 );
  return mapper.checkMap( x, y , 0.25, log(0.6/(1.0 - 0.6)));
}

