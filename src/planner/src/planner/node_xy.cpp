#include <iostream>

#include "planner/node_xy.h"

using namespace std;

Node::Node(): 
         x( 0.0 ), 
         y( 0.0 ), 
         back_ptr( -1 ), 
         f( 0.0 ), 
         g( 0.0 ), 
         h( 0.0 ) 
         {
         }

Node::Node( double x_in, double y_in, int b_ptr, double f_in, double g_in , double h_in): 
        x( x_in ), 
        y( y_in ), 
        back_ptr( b_ptr ), 
        f( f_in ), 
        g( g_in ) {}

Node::~Node(){
  //delete( back_ptr );
}

