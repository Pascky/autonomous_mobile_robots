#include <iostream> 

#include "geometry_msgs/Point.h"

class Node{
  public:
    Node(); 
    Node( double x_in, double y_in, int back_ptr_in, double f_in, double g_in, double h_in );
    virtual ~Node();

    double x;
    double y;
    int back_ptr;
    double f;
    double g;
    double h;
};
