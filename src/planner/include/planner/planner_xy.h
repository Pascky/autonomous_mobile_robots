
#ifndef PLANNER_XY
#define PLANNER_XY

#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>

#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include  "nav_msgs/Odometry.h"
#include  "nav_msgs/Path.h"

#include "std_msgs/UInt32.h"
#include "planner/node_xy.h"
#include "mapper/mapper.h"

#define STR_DIST  0.25   /* Assume distance between two nodes is 0.5 m */
#define DIAG_DIST 0.3125 /* Distance between neighbouring node along the diagonal */
#define N_NEIGHBOURS 8  /* Number of nodes around each node */

class PlannerXY{
  public:
    PlannerXY(); 
/*    PlannerXY( const geometry_msgs::Pose& start, const geometry_msgs::Pose& goal, const double& w ); */
    virtual ~PlannerXY( void );
    
    bool search( const geometry_msgs::Pose& start, const geometry_msgs::Pose& goal, const double& w);
    nav_msgs::Path generate_path( void );
    std_msgs::UInt32 open_list_size( void );
    std_msgs::UInt32 closed_list_size( void );
    double e_distance( geometry_msgs::Point start, geometry_msgs::Point end );
    
    //new subscriber callback functions
    void handle_odom( const nav_msgs::Odometry::ConstPtr& msg );
    void handle_goal( const geometry_msgs::Pose::ConstPtr& msgs );
//    void get_neighbours(Node neighbours[],  Node curr_node, Node goal, double w);
    bool check_map( const double& x, const double& y );
    
    ros::Publisher path_publisher;
    ros::Publisher openlistsize_publisher;
    ros::Publisher closedlistsize_publisher;
    nav_msgs::Odometry odometry;
    double discretization;
    
    Mapper mapper;

  protected:
    std::vector<Node> closed_list;
    std::vector<Node> open_list;      
};

#endif /* PLANNER_XY */
